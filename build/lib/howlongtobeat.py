import sys
from bs4 import BeautifulSoup, Tag
import requests
import re
import traceback
import logging
from difflib import get_close_matches

logger = logging.getLogger('HLTB')


def search_for_match(game, results):

    match = get_close_matches(game, results, 1, 0.9)
    if match:
        return results.index(match[0])
    return -1




class HowLongToBeat():
    def __init__(self,game):
        headers = {'User-Agent': 'Mozilla/5.0'}
        form_data = {"queryString": game,"t": "games",
                     "sorthead": "popular", "sortd": "Normal Order","length_type": "main"}
        url = "https://howlongtobeat.com/search_results.php?page=1"
        request = requests.post(url, data=form_data, headers=headers)
        self.game = game
        self.raw_data = BeautifulSoup(request.text, "html.parser")
        self.raw_time = ""
        self.fulltime = -1
        self.found = False
        self.units = "Hours"
        try:
            links = self.raw_data.find_all("div", attrs={"class": "search_list_details"})
            titles = [tit.find("a")['title'] for tit in links]
            match = search_for_match(self.game, titles)
            if match >= 0:
                self.id = re.search(r"([\d]*$)", links[match].find("a")['href']).group(0)
                self.extract_time_from_result(links[match])
            else:
                logger.info(u"NOT FOUND: {0}.".format(game))
        except:
            logger.error(traceback.print_exc())
            self.id = 0
            logger.error(sys.exc_info()[0])
            logger.error(sys.exc_info()[1])
            raise Exception

    def __str__(self):
        if self.found:
            return u"{0} - {1} {2}".format(self.game, self.fulltime, self.units)
        else:
            return self.game + u" - Not Found"

    def extract_time_from_result(self, result):
        raw_time = result.find("div", attrs={"class": "search_list_details_block"}).contents
        for element in raw_time:
            if isinstance(element, Tag):
                time_text = element.text.replace(u"\xbd", '.5')
                time_search = re.search(r"\d+\.?\d* \w*", time_text)
                if time_search:
                    self.fulltime = float(re.search(r"\d+\.?\d*", time_text).group(0))
                    self.units = re.search(r"(\d+\.?\d*) (\w*)", time_text).group(2)
                    break
                else:
                    self.fulltime = -1.0
                    self.units = "Hours"
                    self.found = True


class ExampleHowLongToBeat:
    def __init__(self, game):
        self.game = game

        with open("examplehltb.html", "r") as f:
            self.raw_data = BeautifulSoup(f.read(), "html.parser")
        try:
            self.id = self.raw_data.find("a", attrs={"title": game})['href'][12:]
            links = self.raw_data.find_all("div", attrs={"class": "search_list_details"})
            for link in links:
                inner_tag = link.find("a", attrs={"title": self.game})
                if inner_tag is not None:
                    raw_time = link.find("div", attrs={"class": "search_list_tidbit center time_100"})
                    time_text = raw_time.text.replace(u"\xbd", '.5')
                    self.fulltime = float(re.search(r"([\d.\d]*)", time_text).group(1))
            self.found = True
        except:
            logger.error(sys.exc_info()[0])
            logger.error(sys.exc_info()[1])
            logger.error(sys.exc_info()[2])
            self.id = 0
            self.fulltime = -1
            self.found = False
            raise Exception

    def __str__(self):
        if self.found:
            return self.game + " - " + str(self.fulltime) + " Hours"
        else:
            return self.game + " - Not Found"

# #def clean_titles():
# with open("titles.txt","r") as f:
#     #with open("systems.txt","r") as g:
#         with open("output.txt","w+") as h:
#             title = True
#             while title:
#                 title = f.readline().strip("\n")
#                 #system = g.readline()
#                 hltb = HowLongToBeat(title)
#                 if hltb.found:
#                     print hltb
#                     h.write(str(hltb))
#                     h.write("\n")
#                 else:
#                     #temp = hltb.raw_data.find("h3",attrs={"class":"head_padding shadow_box back_blue center"})
#                     #print (temp if temp else "{0} not found.".format(title))
#                     pass


#<li class='global_padding back_white shadow_box'>No results for <strong>a mini falafa</strong> in <u>games</u>.</li>
