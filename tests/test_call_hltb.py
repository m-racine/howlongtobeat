import unittest
import os
from howlongtobeat import HowLongToBeat, ExampleHowLongToBeat


class HLTBStandardTests(unittest.TestCase):
    def test_known_good_hltb(self):
        hltb = HowLongToBeat("Human Resource Machine")
        self.assertEqual(hltb.game, "Human Resource Machine")
        self.assertEqual(hltb.fulltime, 4.0)

    def test_known_bad_hltb(self):
        hltb = HowLongToBeat("Legion Saga")
        self.assertEqual(hltb.game, "Legion Saga")
        self.assertEqual(hltb.fulltime, -1)

    def test_alternate_names_hltb(self):
        hltb = HowLongToBeat("Antihero")
        self.assertEqual(str(hltb), "Antihero - Not Found")
        self.assertEqual(hltb.fulltime, -1)

        hltb = HowLongToBeat("Antihero (2017)")
        self.assertEqual(hltb.game, "Antihero (2017)")
        self.assertEqual(6.0, hltb.fulltime)

    def test_example_hltb(self):
        os.chdir("../")
        hltb = ExampleHowLongToBeat("Sunset Overdrive")
        os.chdir("tests")
        self.assertEqual(hltb.game, "Sunset Overdrive")
        self.assertEqual(hltb.fulltime, 10.0)
